﻿using System;
using System.IO;
using System.Web.Script.Serialization;

namespace TrulyQuantumChess.WebApp {
    public class WebAppConfig {
        private static readonly WebAppConfig Instance_;

        public static WebAppConfig Instance {
            get { return Instance_; }
        }

        static WebAppConfig() {
            string input = File.ReadAllText("WebAppConfig.json");
            var jss = new JavaScriptSerializer();
            Instance_ = jss.Deserialize<WebAppConfig>(input);
            Console.Write(Instance_.ListenUrl);
        }

        public string ListenUrl { get; set; }
        public string Prefix { get; set; }
        public MongoConnection Mongo { get; set; }
        public double CleanAfterHours { get; set; }
        public bool Debug { get; set; }
        public PiecesInfo Pieces { get; set; }
        public string DocUrl { get; set; }
        public CaptchaSettings Captcha { get; set; }
    }

    // Helper for dependency injections
    public static class WebAppManagers {
        private static readonly IDatabaseManager DatabaseManager_ =
            new MongoManager();

        public static IDatabaseManager DatabaseManager {
            get { return DatabaseManager_; }
        }
    }

    public class MongoConnection {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }

    public class PiecesInfo {
        public string Collection { get; set; }
        public PiecesWidthRatiosInfo WidthRatios { get; set; }
    }

    public class PiecesWidthRatiosInfo {
        public double Pawn { get; set; }
        public double Knight { get; set; }
        public double Bishop { get; set; }
        public double Rook { get; set; }
        public double Queen { get; set; }
        public double King { get; set; }
    }

    public class CaptchaSettings {
        public bool Enabled { get; set; }
        public string Public { get; set; }
        public string Secret { get; set; }
    }
}